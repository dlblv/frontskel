import van from "vanjs-core/debug";

const { h1 } = van.tags;

van.add(document.body, h1("Hello from VanJS!"));
